package com.android.tlabs.brentit.activity.component;

import com.android.tlabs.brentit.activity.ActivityScope;
import com.android.tlabs.brentit.activity.home.HomeActivity;
import com.android.tlabs.brentit.activity.module.HomeActivityModule;

import dagger.Subcomponent;

/**
 * Created by Ram on 3/09/2016.
 */

@ActivityScope
@Subcomponent (modules = HomeActivityModule.class)
public interface HomeActivityComponent {
    HomeActivity inject(HomeActivity activity);
}
