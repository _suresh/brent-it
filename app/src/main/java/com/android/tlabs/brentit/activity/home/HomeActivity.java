package com.android.tlabs.brentit.activity.home;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.android.tlabs.brentit.BrentApplication;
import com.android.tlabs.brentit.R;
import com.android.tlabs.brentit.activity.feeds.FeedsActivity;
import com.android.tlabs.brentit.activity.module.HomeActivityModule;
import com.android.tlabs.brentit.model.Product;
import com.android.tlabs.brentit.model.ProductCollection;
import com.android.tlabs.brentit.utils.Constants;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class HomeActivity extends AppCompatActivity implements Home_ModelViewPresenter.RequiredHomeViewOps {

    @Bind(R.id.iv_home_icon)
    ImageView iv_home_icon;

    @Bind(R.id.iv_car_icon)
    ImageView iv_car_icon;

    @Bind(R.id.iv_cloth_icon)
    ImageView iv_cloth_icon;

    @Bind(R.id.iv_books_icon)
    ImageView iv_books_icon;

    @Bind(R.id.iv_brain_icon)
    ImageView iv_brain_icon;

    @Bind(R.id.iv_computer_icon)
    ImageView iv_computer_icon;

    @Bind(R.id.iv_music_icon)
    ImageView iv_music_icon;

    @Bind(R.id.iv_misc_icon)
    ImageView iv_misc_icon;

    @Inject
    public Home_ModelViewPresenter.ProvidedHomePresenterOps mHomePresenter;

    public BrentApplication application;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setTheme(R.style.BrandedTheme);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        application = (BrentApplication) getAppContext();

        setupComponent();
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    public void categoryClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_home_icon:
                mHomePresenter.itemClicked(Constants.KEY_CATEGORY_HOME);
                break;
            case R.id.iv_car_icon:
                mHomePresenter.itemClicked(Constants.KEY_CATEGORY_CAR);
                break;
            case R.id.iv_cloth_icon:
                mHomePresenter.itemClicked(Constants.KEY_CATEGORY_CLOTHES);
                break;
            case R.id.iv_computer_icon:
                mHomePresenter.itemClicked(Constants.KEY_CATEGORY_COMPUTER);
                break;
            case R.id.iv_music_icon:
                mHomePresenter.itemClicked(Constants.KEY_CATEGORY_MUSIC);
                break;
            case R.id.iv_books_icon:
                mHomePresenter.itemClicked(Constants.KEY_CATEGORY_BOOKS);
                break;
            case R.id.iv_brain_icon:
                mHomePresenter.itemClicked(Constants.KEY_CATEGORY_BRAIN);
                break;
            case R.id.iv_misc_icon:
                mHomePresenter.itemClicked(Constants.KEY_CATEGORY_MISC);
                break;
            default:
                break;
        }
    }

    private void setupComponent() {
        Timber.d("HomeActivity: setupComponent");
        application.getAppComponent().getHomeComponent(new HomeActivityModule(this)).inject(this);
    }

    @Override
    public void itemClickProcessed(List<Product> productList) {
        Bundle b = new Bundle();
        b.putParcelable("productList", Parcels.wrap(new ProductCollection(productList)));
        Intent startFeedsActivity = new Intent(getApplicationContext(), FeedsActivity.class);
        startFeedsActivity.putExtra("ProductData", b);
        startActivity(startFeedsActivity);
    }

    @Override
    public void showErrorMessage(String message) {
        Toast.makeText(getActivityContext(), message, Toast.LENGTH_SHORT).show();
    }
}
