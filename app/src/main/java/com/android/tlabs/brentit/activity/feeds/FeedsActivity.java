package com.android.tlabs.brentit.activity.feeds;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;

import com.android.tlabs.brentit.R;
import com.android.tlabs.brentit.adapter.FeedsAdapter;
import com.android.tlabs.brentit.model.Product;
import com.android.tlabs.brentit.model.ProductCollection;

import org.parceler.Parcels;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import timber.log.Timber;

public class FeedsActivity extends AppCompatActivity implements Feeds_ModelViewPresenter.RequiredFeedsViewOps{

    @Bind(R.id.tool_bar)
    Toolbar toolbar;

    @Bind(R.id.rv_feedsList)
    RecyclerView rv_feedsList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeds);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        toolbar.setBackgroundColor(Color.parseColor("#D25565"));
        getSupportActionBar().setTitle(null);

        Drawable upArrow = ContextCompat.getDrawable(getActivityContext(), R.drawable.abc_ic_ab_back_mtrl_am_alpha);
        upArrow.setColorFilter(ContextCompat.getColor(getActivityContext(), R.color.white), PorterDuff.Mode.SRC_ATOP);
        getSupportActionBar().setHomeAsUpIndicator(upArrow);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        fetchProductInformation();
    }

    private void fetchProductInformation() {
        Timber.d("FeedsActivity:fetchProductInformation");
        Bundle b = getIntent().getBundleExtra("ProductData");
        ProductCollection pCollection = Parcels.unwrap(b.getParcelable("productList"));

        setProductList(pCollection.getProductList());
    }

    private void setProductList(List<Product> productList) {
        Timber.d("Product list size:"+productList.size());
        LinearLayoutManager llm = new LinearLayoutManager(getActivityContext());
        rv_feedsList.setLayoutManager(llm);
        FeedsAdapter adapter = new FeedsAdapter(getActivityContext(), productList);
        rv_feedsList.setAdapter(adapter);
    }

    @Override
    public Context getActivityContext() {
        return this;
    }

    @Override
    public Context getAppContext() {
        return getApplicationContext();
    }
}
