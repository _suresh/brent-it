package com.android.tlabs.brentit.activity.component;

import android.app.Application;

import com.android.tlabs.brentit.activity.module.AppModule;
import com.android.tlabs.brentit.activity.module.HomeActivityModule;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by Ram on 3/09/2016.
 */

@Singleton
@Component( modules = AppModule.class)
public interface AppComponent {
    Application application();
    HomeActivityComponent getHomeComponent(HomeActivityModule module);
}
