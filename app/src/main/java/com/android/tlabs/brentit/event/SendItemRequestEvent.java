package com.android.tlabs.brentit.event;

/**
 * Created by sureshchidambaram on 6/27/2016.
 */

public class SendItemRequestEvent {
    private int itemPosition;
    public SendItemRequestEvent(int itemPosition){
        this.itemPosition = itemPosition;
    }
    public int getItemPosition(){
        return itemPosition;
    }
}
