package com.android.tlabs.brentit.event;

/**
 * Created by sureshchidambaram on 6/27/2016.
 */

public class UpdateLikeEvent {
    private int itemPosition;
    public UpdateLikeEvent(int itemPosition){
        this.itemPosition = itemPosition;
    }
    public int getItemPosition(){
        return itemPosition;
    }
}