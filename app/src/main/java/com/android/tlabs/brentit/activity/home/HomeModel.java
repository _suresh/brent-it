package com.android.tlabs.brentit.activity.home;

import com.android.tlabs.brentit.BrentApplication;
import com.android.tlabs.brentit.api.IBrentAPI;
import com.android.tlabs.brentit.model.Category;
import com.android.tlabs.brentit.model.Product;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

/**
 * Created by Ram on 3/09/2016.
 */

public class HomeModel implements Home_ModelViewPresenter.ProvidedHomeModelOps {

    private Home_ModelViewPresenter.RequiredHomePresenterOps mHomePresenter;

    private IBrentAPI apiService;

    private BrentApplication application;

    private List<Product> pList;

    public HomeModel(Home_ModelViewPresenter.RequiredHomePresenterOps homePresenterOps) {
        mHomePresenter = homePresenterOps;
    }

    @Override
    public void getProductList(Category selectedCategory) {
        Timber.d("HomeModel: getProductList");
        application = (BrentApplication) mHomePresenter.getAppContext();
        apiService = application.getBrentAPI();
        pList = new ArrayList<>();
        Call<List<Product>> feedsResponseCall = apiService.getProductsList(selectedCategory.getCategoryId());
        feedsResponseCall.enqueue(new Callback<List<Product>>() {
            @Override
            public void onResponse(Call<List<Product>> call, Response<List<Product>> response) {
                Timber.d(response.message());
                pList  = new ArrayList<>();
                pList = response.body();
                mHomePresenter.productListForCategory(pList);
            }

            @Override
            public void onFailure(Call<List<Product>> call, Throwable t) {
                Timber.d(t.getMessage());
                mHomePresenter.productListForCategory(null);
            }
        });

    }
}
