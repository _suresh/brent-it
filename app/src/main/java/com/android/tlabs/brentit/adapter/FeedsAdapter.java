package com.android.tlabs.brentit.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.android.tlabs.brentit.R;
import com.android.tlabs.brentit.model.Product;

import java.util.List;

import timber.log.Timber;

/**
 * Created by Ram on 12/09/2016.
 */

public class FeedsAdapter extends RecyclerView.Adapter<FeedsAdapter.FeedsViewHolder>{

    private List<Product> mProductList;

    private Context mContext;

    public class FeedsViewHolder extends RecyclerView.ViewHolder {
        TextView tv_productName, tv_productAvailability, tv_postedBy;

        public FeedsViewHolder(View v) {
            super(v);
            tv_productName = (TextView) v.findViewById(R.id.tv_productName);
            tv_productAvailability = (TextView) v.findViewById(R.id.tv_productAvailability);
            tv_postedBy = (TextView) v.findViewById(R.id.tv_postedBy);
        }
    }

    public FeedsAdapter(Context context, List<Product> productList) {
        mContext = context;
        mProductList = productList;
    }

    @Override
    public FeedsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View cardView;
        if (viewType == 0) {
            //rent card view
            cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_rent_feeds, parent, false);
        } else {
            //borrow card view
            cardView = LayoutInflater.from(parent.getContext()).inflate(R.layout.card_borrow_feeds, parent, false);
        }

        return new FeedsViewHolder(cardView);
    }

    @Override
    public int getItemCount() {
        return mProductList.size();
    }

    @Override
    public void onBindViewHolder(FeedsViewHolder holder, int position) {
        holder.tv_productName.setText(mProductList.get(position).getDescription());
        holder.tv_productAvailability.setText(mProductList.get(position).getStatus());
        holder.tv_postedBy.setText("Posted by "+mProductList.get(position).getUserID());
    }

    @Override
    public int getItemViewType(int position) {
        Timber.d("getItemViewType:"+position+":"+mProductList.get(position).getBorrowRent());

        return mProductList.get(position).getBorrowRent();
    }
}
