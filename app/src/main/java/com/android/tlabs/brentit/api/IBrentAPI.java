package com.android.tlabs.brentit.api;

import com.android.tlabs.brentit.model.Product;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Ram on 26/01/2016.
 */
public interface IBrentAPI {

    @GET("product/findProduct")
    Call<List<Product>> getProductsList(@Query("categoryID") String categoryId);

}
