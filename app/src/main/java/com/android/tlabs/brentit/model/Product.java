package com.android.tlabs.brentit.model;

import org.parceler.Parcel;

import java.util.List;

/**
 * Created by Ram on 3/09/2016.
 */
@Parcel
public class Product {

    String _id;

    String categoryID;

    String productID;

    String description;

    int borrowRent;

    String userID;

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String productID) {
        this.productID = productID;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getBorrowRent() {
        return borrowRent;
    }

    public void setBorrowRent(int borrowRent) {
        this.borrowRent = borrowRent;
    }

    public String getUserID() {
        return userID;
    }

    public void setUserID(String userID) {
        this.userID = userID;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getLocale() {
        return locale;
    }

    public void setLocale(String locale) {
        this.locale = locale;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int get__v() {
        return __v;
    }

    public void set__v(int __v) {
        this.__v = __v;
    }

    String timeStamp;

    String locale;

    String status;

    int __v;
}
