package com.android.tlabs.brentit.model;

import org.parceler.Parcel;
import org.parceler.ParcelConstructor;

import java.util.List;

/**
 * Created by Ram on 12/09/2016.
 */
@Parcel
public class ProductCollection {

    @ParcelConstructor
    public ProductCollection(List<Product> productList) {
        this.productList = productList;
    }

    public List<Product> getProductList() {
        return productList;
    }

    List<Product> productList;


}
