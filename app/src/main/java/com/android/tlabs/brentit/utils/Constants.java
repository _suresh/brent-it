package com.android.tlabs.brentit.utils;

/**
 * Created by sureshchidambaram on 04/06/16.
 */

public class Constants {

    public static final String KEY_CATEGORY_HOME = "C001";
    public static final String KEY_CATEGORY_CAR = "C002";
    public static final String KEY_CATEGORY_CLOTHES = "C003";
    public static final String KEY_CATEGORY_COMPUTER = "C004";
    public static final String KEY_CATEGORY_MUSIC = "C005";
    public static final String KEY_CATEGORY_BOOKS = "C006";
    public static final String KEY_CATEGORY_BRAIN = "C007";
    public static final String KEY_CATEGORY_MISC = "C008";

}
