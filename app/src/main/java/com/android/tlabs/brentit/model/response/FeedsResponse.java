package com.android.tlabs.brentit.model.response;

/**
 * Created by sureshchidambaram on 04/06/16.
 */

public class FeedsResponse {
    private String userID;
    private String userName;
    private String itemTitle;
    private String timeStamp;
    private boolean isRequest, isLiked;
    private int likesCount;

    public boolean isRequest() {
        return isRequest;
    }

    public String getUserName() {
        return userName;
    }

    public String getUserID() {
        return userID;
    }

    public int getLikesCount() {
        return likesCount;
    }

    public void updateLikesCount(){
        if (isLiked()) {
            likesCount--;
            setLiked(false);
        } else{
            likesCount++;
            setLiked(true);
        }
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String getItemTitle() {
        return itemTitle;
    }

    public boolean isLiked() {
        return isLiked;
    }

    //change to private access after sqlite/firebse integration
    public void setLiked(boolean liked) {
        isLiked = liked;
    }

    //remove the below setters after sqlite/firebase integration
    public void setUserID(String userID) {
        this.userID = userID;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setItemTitle(String itemTitle) {
        this.itemTitle = itemTitle;
    }

    public void setTimeStamp(String timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setRequest(boolean request) {
        isRequest = request;
    }

    public void setLikesCount(int likesCount) {
        this.likesCount = likesCount;
    }
}
