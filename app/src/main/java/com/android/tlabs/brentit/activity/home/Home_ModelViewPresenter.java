package com.android.tlabs.brentit.activity.home;

import android.content.Context;
import android.widget.ImageView;

import com.android.tlabs.brentit.model.Category;
import com.android.tlabs.brentit.model.Product;

import java.util.List;

/**
 * Created by Ram on 3/09/2016.
 */

public interface Home_ModelViewPresenter {

    /** View operations available for the presenter */
    interface RequiredHomeViewOps {
        Context getAppContext();
        Context getActivityContext();
        void itemClickProcessed(List<Product> productList);
        void showErrorMessage(String message);
    }

    /** Presenter operations available for the View */
    interface  ProvidedHomePresenterOps {
        void itemClicked(String categoryCode);
        void setView(RequiredHomeViewOps view);
    }

    /** Presenter operations available for the Model */
    interface RequiredHomePresenterOps {
        Context getAppContext();
        Context getActivityContext();
        void productListForCategory(List<Product> productList);
    }

    /** Model operations available for the presenter */
    interface ProvidedHomeModelOps {
        void getProductList(Category selectedCategory);
    }
}
