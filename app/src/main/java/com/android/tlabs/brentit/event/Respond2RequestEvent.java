package com.android.tlabs.brentit.event;

/**
 * Created by sureshchidambaram on 6/27/2016.
 */

public class Respond2RequestEvent {
    private int itemPosition;
    public Respond2RequestEvent(int itemPosition){
        this.itemPosition = itemPosition;
    }
    public int getItemPosition(){
        return itemPosition;
    }
}
