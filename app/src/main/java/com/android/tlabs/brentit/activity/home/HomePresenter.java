package com.android.tlabs.brentit.activity.home;

import android.content.Context;
import android.widget.ImageView;

import com.android.tlabs.brentit.R;
import com.android.tlabs.brentit.model.Category;
import com.android.tlabs.brentit.model.Product;

import java.lang.ref.WeakReference;
import java.util.List;

import timber.log.Timber;

/**
 * Created by Ram on 3/09/2016.
 */

public class HomePresenter implements Home_ModelViewPresenter.ProvidedHomePresenterOps, Home_ModelViewPresenter.RequiredHomePresenterOps {

    private WeakReference<Home_ModelViewPresenter.RequiredHomeViewOps> mView;

    private Home_ModelViewPresenter.ProvidedHomeModelOps mHomeModel;

    public HomePresenter(Home_ModelViewPresenter.RequiredHomeViewOps homeView) {
        mView = new WeakReference<>(homeView);
    }

    private Home_ModelViewPresenter.RequiredHomeViewOps getView() throws NullPointerException {
        if (mView != null) {
            return mView.get();
        } else {
            throw  new NullPointerException("View unavailable");
        }
    }

    @Override
    public void setView(Home_ModelViewPresenter.RequiredHomeViewOps view) {
        mView = new WeakReference<>(view);
    }

    public void setModel(Home_ModelViewPresenter.ProvidedHomeModelOps model) {
        mHomeModel = model;
    }


    @Override
    public void productListForCategory(List<Product> productList) {
        List<Product> pList = productList;

        if (pList == null) {
            //show error message
            getView().showErrorMessage(getActivityContext().getString(R.string.product_fetch_error_msg));
        } else {
            getView().itemClickProcessed(pList);
        }
    }

    @Override
    public void itemClicked(String categoryCode) {
        Timber.d("HomePresenter: itemClicked");

        if (categoryCode != null && categoryCode.trim().length() != 0) {
            mHomeModel.getProductList(getCategory(categoryCode));
        } else {
            Timber.d("HomePresenter: itemClicked:: invalid category code");
        }
    }

    @Override
    public Context getAppContext() {
        try {
            return getView().getAppContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    @Override
    public Context getActivityContext() {
        try {
            return getView().getActivityContext();
        } catch (NullPointerException e) {
            return null;
        }
    }

    private Category getCategory(String categoryCode) {
        Category productCategory = new Category();
        productCategory.setCategoryId(categoryCode);

        return productCategory;
    }
}
