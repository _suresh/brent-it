package com.android.tlabs.brentit.utils;

import android.content.Context;
import android.graphics.Typeface;

/**
 * Created by Ram on 11/08/2016.
 */

public class Fonts {

    public static Typeface getBlack(Context cx) {
        return Typeface.createFromAsset(cx.getAssets(), "fonts/corpidc1-black-webfont.ttf");
    }

    public static Typeface getBold(Context cx) {
        return Typeface.createFromAsset(cx.getAssets(), "fonts/corpidc1-bold-webfont.ttf");
    }

    public static Typeface getHeavy(Context cx) {
        return Typeface.createFromAsset(cx.getAssets(), "fonts/corpidc1-heavy-webfont.ttf");
    }

    public static Typeface getLight(Context cx) {
        return Typeface.createFromAsset(cx.getAssets(), "fonts/corpidc1-light-webfont.ttf");
    }

    public static Typeface getRegular(Context cx) {
        return Typeface.createFromAsset(cx.getAssets(), "fonts/corpidc1-regular-webfont.ttf");
    }

}
