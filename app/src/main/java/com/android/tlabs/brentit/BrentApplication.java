package com.android.tlabs.brentit;

import android.app.Application;

import com.android.tlabs.brentit.activity.component.AppComponent;
import com.android.tlabs.brentit.activity.component.DaggerAppComponent;
import com.android.tlabs.brentit.activity.module.AppModule;
import com.android.tlabs.brentit.api.IBrentAPI;
import com.android.tlabs.brentit.api.RestClient;

import com.crashlytics.android.Crashlytics;
import com.digits.sdk.android.Digits;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterCore;
import io.fabric.sdk.android.Fabric;
import timber.log.Timber;

/**
 * Created by Ram on 3/07/2016.
 */
public class BrentApplication extends Application {

    // Note: Your consumer key and secret should be obfuscated in your source code before shipping.
    private static final String TWITTER_KEY = "my7X2L83Fua95NHNVy98vJ0p8";
    private static final String TWITTER_SECRET = "lgQrZvViBtou8xZgF5T2xXPP0eM718hxEzqWTo4qjcVQJTnLvj";
    
    public IBrentAPI brentAPI;

    private AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        initAppComponent();
        TwitterAuthConfig authConfig = new TwitterAuthConfig(TWITTER_KEY, TWITTER_SECRET);
        Fabric.with(this, new Crashlytics(), new TwitterCore(authConfig), new Digits.Builder().build());
        RestClient client = new RestClient();
        brentAPI = client.getAPIService();

        if (BuildConfig.DEBUG) {
            Timber.plant(new Timber.DebugTree());
        }

    }

    public IBrentAPI getBrentAPI() {

        return brentAPI;
    }

    private void initAppComponent() {
        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
