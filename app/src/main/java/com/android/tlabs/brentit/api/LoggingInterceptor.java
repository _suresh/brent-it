package com.android.tlabs.brentit.api;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;
import timber.log.Timber;

/**
 * Created by Ram on 16/05/2016.
 */
public class LoggingInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        Timber.d(request.url().toString());

        long t1 = System.nanoTime();
        /*Timber.i(String.format("Sending request %s on %s%n%s",
                request.url(), chain.connection(), request.body().toString()));*/

        Response response = chain.proceed(request);

        long t2 = System.nanoTime();
        Timber.i(String.format("Received response for %s in %.1fms%n%s",
                response.request().url(), (t2 - t1) / 1e6d, response.headers()));

        return response;
    }
}
