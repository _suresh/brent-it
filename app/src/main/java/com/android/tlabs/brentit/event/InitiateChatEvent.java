package com.android.tlabs.brentit.event;

/**
 * Created by sureshchidambaram on 6/27/2016.
 */

public class InitiateChatEvent {
    private int itemPosition;
    public InitiateChatEvent(int itemPosition){
        this.itemPosition = itemPosition;
    }
    public int getItemPosition(){
        return itemPosition;
    }
}
