package com.android.tlabs.brentit.event;

/**
 * Created by sureshchidambaram on 6/28/2016.
 */

public class ReportAbuseEvent {
    private int itemPosition;
    public ReportAbuseEvent(int itemPosition){
        this.itemPosition = itemPosition;
    }
    public int getItemPosition(){
        return itemPosition;
    }
}
