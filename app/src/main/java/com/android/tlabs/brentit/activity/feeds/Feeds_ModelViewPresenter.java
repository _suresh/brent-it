package com.android.tlabs.brentit.activity.feeds;

import android.content.Context;

import com.android.tlabs.brentit.model.Product;

import java.util.List;

/**
 * Created by Ram on 12/09/2016.
 */

public interface Feeds_ModelViewPresenter {

    /** View operations available for the presenter */
    interface RequiredFeedsViewOps {
        Context getAppContext();
        Context getActivityContext();
    }

    /** Presenter operations available for the View */
    interface ProvidedFeedsPresenterOps {
        void setView(RequiredFeedsViewOps view);
    }

    /** Presenter operations available for the Model */
    interface RequiredFeedsPresenterOps {
        Context getAppContext();
        Context getActivityContext();
    }

    /** Model operations available for the presenter */
    interface ProvidedFeedsModelOps {

    }
}
