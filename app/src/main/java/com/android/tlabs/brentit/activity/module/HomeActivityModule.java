package com.android.tlabs.brentit.activity.module;

import com.android.tlabs.brentit.activity.ActivityScope;
import com.android.tlabs.brentit.activity.home.HomeActivity;
import com.android.tlabs.brentit.activity.home.HomeModel;
import com.android.tlabs.brentit.activity.home.HomePresenter;
import com.android.tlabs.brentit.activity.home.Home_ModelViewPresenter;

import dagger.Module;
import dagger.Provides;

/**
 * Created by Ram on 3/09/2016.
 */
@Module
public class HomeActivityModule {

    private HomeActivity homeActivity;

    public HomeActivityModule(HomeActivity activity) {
        this.homeActivity = activity;
    }

    @Provides
    @ActivityScope
    HomeActivity providesHomeActivity() {
        return homeActivity;
    }

    @Provides
    @ActivityScope
    Home_ModelViewPresenter.ProvidedHomePresenterOps providedHomePresenterOps() {
        HomePresenter homePresenter = new HomePresenter(homeActivity);
        HomeModel homeModel = new HomeModel(homePresenter);
        homePresenter.setModel(homeModel);

        return homePresenter;
    }
}
