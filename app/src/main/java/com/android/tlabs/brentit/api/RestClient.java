package com.android.tlabs.brentit.api;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Ram on 11/04/2016.
 */
public class RestClient {

    public static final String BASE_URL = "http://192.168.0.22:3000";

    public IBrentAPI brentAPI;

    private OkHttpClient okHttpClient;

    public IBrentAPI getAPIService() {

        return brentAPI;
    }

    public RestClient() {
        okHttpClient = new OkHttpClient.Builder().addInterceptor(new LoggingInterceptor()).build();
        Retrofit retrofit = new Retrofit.Builder().baseUrl(BASE_URL).client(okHttpClient).addConverterFactory(GsonConverterFactory.create()).build();
        brentAPI = retrofit.create(IBrentAPI.class);
    }

}
