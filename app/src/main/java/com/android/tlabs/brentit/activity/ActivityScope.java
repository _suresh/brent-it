package com.android.tlabs.brentit.activity;

import javax.inject.Scope;

/**
 * Created by Ram on 3/09/2016.
 */
@Scope
public @interface ActivityScope {
}
